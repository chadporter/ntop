#!/bin/bash
ZEEKVERSION="4.2.2"

echo "This script will create .deb packages for Zeek"
echo
echo "Installing: build-essential devscripts debhelper "
echo

sudo -E apt-get update
sudo -E apt-get install -y build-essential devscripts debhelper autoconf automake autotools-dev dh-make

echo
echo "Installing: Zeek dependiances "
echo
echo

sudo -E apt-get install -y cmake make gcc g++ flex bison fakeroot xutils lintian pbuilder pkg-config libpcre3-dev libyaml-dev libhtp-dev libpcap-dev libssl-dev python-dev swig zlib1g-dev libmaxminddb-dev sendmail curl libgoogle-perftools-dev jemalloc-dev libkrb5-dev

echo "Versions of installed packages"
echo
dpkg -l | egrep "build-essential|devscripts|debhelper"
echo
echo "Creating directories to build packages"
echo
mkdir -p ~/build/zeek
echo
echo "Building & installing Zeek $ZEEKVERSION"

cd ~/build/zeek
wget https://download.zeek.org/zeek-$ZEEKVERSION.tar.gz
#exit 0

mv zeek-$ZEEKVERSION.tar.gz zeek_$ZEEKVERSION.orig.tar.gz
tar xf zeek_$ZEEKVERSION.orig.tar.gz
cd zeek-$ZEEKVERSION
mkdir debian
dch --create -v $ZEEKVERSION --package zeek
echo 9 > debian/compat
cat << EOF > debian/control

Source: zeek
Maintainer: zeek.org
Section: misc
Priority: optional
Standards-Version: 2.5

Package: zeek
Architecture: any
Depends: libpcap0.8, \${shlibs:Depends}, \${misc:Depends}
Description: Zeek $ZEEKVERSION

EOF
touch debian/copyright
cat << EOF > debian/rules
#!/usr/bin/make -f

%:

	dh \$@

override_dh_auto_configure:

	./configure

override_dh_usrlocal:

override_dh_shlibdeps:

	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info

EOF
cat << EOF > debian/postinst

#!/bin/sh

# Automatically added by dh_makeshlibs

if [ "\$1" = "configure" ]; then

       /sbin/ldconfig

       echo "creating user zeek"

       adduser --system --group --no-create-home --quiet zeek

       echo "setting permissions"

       setcap cap_net_raw,cap_net_admin=eip /usr/local/zeek/bin/zeek

       chown -R zeek:zeek /usr/local/zeek

fi







ldconfig

EOF
mkdir debian/source
echo "3.0 (quilt)" > debian/source/format
debuild -us -uc

#sudo dpkg -i ../zeek_$ZEEKVERSION_amd64.deb
