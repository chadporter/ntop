#!/bin/bash
apt update -y

dpkg -i -y ndpi_3.0.0-2242_amd64.deb
apt -y --fix-broken install

dpkg -i -y pfring_7.4.0-2835_amd64.deb
apt -y --fix-broken install

dpkg -i -y pfring-dkms_7.4.0_all.deb
apt -y --fix-broken install

dpkg -i -y pfring-drivers-zc-dkms_1.3_all.deb
apt -y --fix-broken install

dpkg -i -y e1000e-zc-dkms_3.4.0.2.2835_all.deb && dpkg -i -y fm10k-zc-dkms_0.23.5.2835_all.deb && dpkg -i -y i40e-zc-dkms_2.4.6.2835_all.deb

dpkg -i -y igb-zc-dkms_5.3.5.18.2835_all.deb && dpkg -i -y ixgbe-zc-dkms_5.3.7.2835_all.deb && dpkg -i -y ixgbevf-zc-dkms_4.5.1.2835_all.deb

dpkg -i -y n2disk_3.4.200207-5184_amd64.deb
apt -y --fix-broken install

dpkg -i -y nprobe_8.6.200207-6343_amd64.deb
apt -y --fix-broken install