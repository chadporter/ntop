#!/bin/bash

# Deb Builder Dependencies
apt install -y dkms ethtool build-essential devscripts debhelper autoconf automake autotools-dev dh-make

# Zeek Dependencies
apt install -y cmake make gcc g++ flex bison fakeroot xutils lintian pbuilder pkg-config libpcre3-dev libyaml-dev libhtp-dev libpcap-dev libssl-dev python3 python3-dev swig zlib1g-dev libmaxminddb-dev sendmail curl libgoogle-perftools-dev libjemalloc-dev libkrb5-dev python3-git python3-semantic-version

# PFRing Dependencies
apt install -y net-tools kmod libkmod-dev ethtool pciutils

# nBox Dependencies
apt install -y apache2 perl libtemplate-perl libcgi-pm-perl libxml-parser-perl libhtml-template-perl libjson-perl libyaml-perl libnet-netmask-perl libnet-cidr-perl liburi-encode-perl tshark dmidecode xfsprogs htop ifenslave hwloc ntp lsscsi apache2-utils wireless-tools iw hostapd rfkill snmp snmpd vlan iptables-persistent libnetwork-ipv4addr-perl rrdtool python3-minimal redis-server parted

# nTopNG Dependencies
apt install -y lsb-release udev debconf bridge-utils tar ethtool libsqlite3-0 redis-server librrd8 logrotate libcurl4 libcurl4-openssl-dev libpcap0.8 libldap-2.5-0 libhiredis0.14 libssl3 libmysqlclient21 librdkafka1 libcap2 libnetfilter-conntrack3 libzstd1 libmaxminddb0 libradcli4 libjson-c5 libnuma1 libzmq5 libnetfilter-queue1 libmaxminddb0 libxml2-dev libglib2.0-0 libglib2.0-dev libsqlite3-dev libmysqlclient-dev libtool libjs-uglify

# nTopNG-Data Dependencies
apt install -y libgeoip-dev libgeoip1

# n2Disk Dependencies
apt install -y libnuma1 libnl-3-200 libpam0g libcurl4 libzmq5

# nDPI Dependencies
apt install -y libnuma1

# nProbe Dependencies
apt install -y libsqlite3-0 libhiredis0.14 libmysqlclient21 libmaxminddb0 libcurl4 librdkafka1 libcap2 libjson-c5 redis-server libnuma1 libnl-3-200 libnetfilter-queue1 debconf libzmq5

# nScrub Dependencies
apt install -y libhiredis0.14 redis-server libssl3 libcurl4 librrd8 python3-redis jshon

# Clone libcaf sources
git clone -b 0.16.5 https://github.com/actor-framework/actor-framework /root/actor-framework_0-16-5
git clone -b 0.17.4 https://github.com/actor-framework/actor-framework /root/actor-framework_0-17-4
git clone https://github.com/actor-framework/actor-framework /root/actor-framework_latest
