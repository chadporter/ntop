#!/bin/bash
SURICATAVERSION="6.0.0"

echo "This script will create .deb packages for Suricata with pf_ring support"
echo
echo "Installing: build-essential autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils lintian pbuilder "
echo

sudo -E apt-get update
sudo -E apt-get install -y build-essential autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils lintian pbuilder pkg-config libpcre3-dev python-dev libyaml-dev libpcap-dev libhtp-dev zlib1g-dev

echo
echo "Installing: Suricata dependencies"
echo
echo

sudo -E apt-get install -y libc6 libcap-ng0 libjansson4 libjansson-dev libluajit-5.1-2 libmagic1 libnet1 libnetfilter-log1 libnetfilter-queue1 libnfnetlink0 libnspr4 libpcap0.8-dev libpcre3 libprelude23 libyaml-0-2 python zlib1g libmagic-dev python-yaml rustc cargo libevent-dev libhiredis-dev liblua5.1-dev libgeoip-dev libnss3-dev libcap-ng-dev liblz4-dev python3-distutils

echo "Installing: PF_Ring for Libraries"

while true; do
	read -p "Path to pfring deb: " PFRING_PATH
	if [ -f $PFRING_PATH ]; then
		break
	fi
done

sudo dpkg -i $PFRING_PATH
sudo -E apt-get --fix-broken install -y

echo "Versions of installed packages"
echo
dpkg -l | egrep "build-essential|devscripts|debhelper"
echo
echo "Creating directories to build packages"
echo
mkdir -p ~/build/suricata
echo
echo "Building & installing Suricata $SURICATAVERSION with pf_ring support"

cd ~/build/suricata
wget http://www.openinfosecfoundation.org/download/suricata-$SURICATAVERSION.tar.gz
mv suricata-$SURICATAVERSION.tar.gz suricata_$SURICATAVERSION.orig.tar.gz
tar -xvzf suricata_$SURICATAVERSION.orig.tar.gz
cd suricata-$SURICATAVERSION
dh_make -s -c gpl2 -n -e DLISNETWORKSECURITYO@exchange.principal.com -f ../suricata-$SURICATAVERSION.tar.gz -y
dpkg-depcheck -d ./configure

cat <<EOF > debian/compat
10
EOF

cat << EOF > debian/control
Source: suricata
Maintainer: suricata
Section: misc
Priority: optional
Standards-Version: 3.9.3
Build-Depends: debhelper (>= 9)

Package: suricata
Architecture: any
Depends: \${shlibs:Depends}, \${misc:Depends}
Description: Suricata open source multi-thread IDS\/IPS/p The Suricata Engine is an Open Source Next Generation Intrusion Detection and Prevention Engine. This engine is not intended to just replace or emulate the existing tools in the industry, but will bring new ideas and  technologies to the field. OISF is part of and funded by the Department of Homeland Security\'s Directorate for Science and Technology HOST program (Homeland Open Security Technology), by the Navy's Space and Naval Warfare Systems Command (SPAWAR), as well as through the very generous support of the members of the OISF Consortium. More information about the Consortium is available, as well as a list of our current Consortium Members. The Suricata Engine and the HTP Library are available to use under the GPLv2./p
EOF

cat << EOF > debian/rules
#!/usr/bin/make -f
%:
	dh \$@
override_dh_auto_configure:
	./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-pfring --with-libpfring-includes=/usr/local/pfring/include --with-libpfring-libraries=/usr/local/pfring/lib --disable-gccmarch-native
override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info
override_dh_auto_install:
	dh_auto_install
	\$(MAKE) install-conf DESTDIR=\$\$(pwd)/debian/suricata
EOF

#dpkg-source --commit

debuild -us -uc
