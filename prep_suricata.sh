#!/bin/bash

apt update -y

apt install -y build-essential autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils lintian pbuilder pkg-config libpcre3-dev python3-dev libyaml-dev libpcap-dev libhtp-dev zlib1g-dev

apt install -y libc6 libcap-ng0 libjansson4 libjansson-dev libluajit-5.1-2 libmagic1 libnet1 libnetfilter-log1 libnetfilter-queue1 libnfnetlink0 libnspr4 libpcap-dev libpcre3 libprelude28 libyaml-0-2 python3 zlib1g libmagic-dev python3-yaml rustc cargo libevent-dev libhiredis-dev liblua5.1-0-dev libgeoip-dev libnss3-dev libcap-ng-dev liblz4-dev python3-distutils libnetfilter-log-dev libnetfilter-queue-dev libnfnetlink-dev libnet1-dev libpcre3-dbg

mkdir -p /root/ntop/20220708
mkdir -p /root/ntop/20220708/all
mkdir -p /root/ntop/20220708/x64

wget https://packages.ntop.org/apt-stable/22.04/x64/InRelease -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/Packages -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/Packages.gz -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/Release -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/Release.gpg -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/cento_1.16.220708-738_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/n2disk_3.6.220708-5272_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/n2n_3.0.0-1044_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/ndpi_4.4.0-3829_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/nprobe-dev_10.0.220708-7851_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/nprobe_10.0.220708-7851_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/nprobes_10.0.220708-7851_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/nscrub_1.4.220708-864_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/ntop-license_1.0_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/ntopng_5.4.220708-18052_amd64.deb -P /root/ntop/20220708/x64
wget https://packages.ntop.org/apt-stable/22.04/x64/pfring_8.2.0-7626_amd64.deb -P /root/ntop/20220708/x64

wget https://packages.ntop.org/apt-stable/22.04/all/InRelease -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/Packages -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/Packages.gz -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/Release -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/Release.gpg -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/apt-ntop-stable.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/e1000e-zc-dkms_3.8.7.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/fm10k-zc-dkms_0.23.5.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/i40e-zc-dkms_2.17.4.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/ice-zc-dkms_1.8.8.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/igb-zc-dkms_5.10.2.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/ixgbe-zc-dkms_5.5.3.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/ixgbevf-zc-dkms_4.5.1.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/ntopng-data_5.4.220708_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/pfring-dkms_8.2.0.7626_all.deb -P /root/ntop/20220708/all
wget https://packages.ntop.org/apt-stable/22.04/all/pfring-drivers-zc-dkms_8.2.0-7626_all.deb -P /root/ntop/20220708/all
