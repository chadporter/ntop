#!/bin/sh
apt-get update
apt-get upgrade -y
apt-get install -y\
    git\
    git-extras\
    vim\
    unattended-upgrades\
    fail2ban\
    logwatch\
    apt-transport-https\
    ca-certificates\
    curl\
    wget\
    gnupg-agent\
    software-properties-common

useradd cporter
mkdir /home/cporter
usermod -s /bin/bash cporter
usermod -aG sudo cporter

mkdir /home/cporter/.ssh
chmod 700 /home/cporter/.ssh
chmod 400 /home/cporter/.ssh/authorized_keys
chown cporter:cporter /home/cporter -R

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update -y
apt install -y\
    docker-ce

curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

usermod -aG docker cporter
curl -s https://install.zerotier.com | bash
